// POST /games - REQUEST
const postNewGameRequest = {
  data: {
    games: [
      {
        players: [
          {
            id: 'player1Id',
            order: 1
          },
          {
            id: 'player2Id',
            order: 2
          },
          {
            id: 'player3Id',
            order: 3
          }
        ]
      }
    ]
  },
  errors: [
    {
      code: '',
      mesage: '',
      type: 'INFO/WARN/ERROR'
    }
  ]
};

// POST /games - RESPONSE

const postNewGameResponse = {
    "data": {
        "games": [
            {
                "id": 339,
                "createdAt": "2020-07-01T04:01:22.498Z",
                "updatedAt": "2020-07-01T04:01:22.498Z",
                "Name": "Game1",
                "isClosed": false,
                "currentRound": [
                    {
                        "roundNumber": 1,
                        "status": "New",
                        "players": [
                            {
                                "id": 1,
                                "name": "player1",
                                "email": "player1@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 1,
                                "bid": null,
                                "score": null,
                                "overAllScore": 0
                            },
                            {
                                "id": 7,
                                "name": "player7",
                                "email": "player7@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 2,
                                "bid": null,
                                "score": null,
                                "overAllScore": 0
                            },
                            {
                                "id": 3,
                                "name": "player3",
                                "email": "player3@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 3,
                                "bid": null,
                                "score": null,
                                "overAllScore": 0
                            }
                        ]
                    }
                ],
                "winnerId": null,
                "numRounds": 17
            }
        ]
    },
    "errors": ""
}


// GET /games?sort=-createdAt
const getGamesResponse = {
  data: {
    games: [
      {
        id: 'game1Id',
        name: '',
        createdAt: '2020-05-20T00:05:32.000Z',
        updatedAt: '2020-05-20T00:05:32.000Z',
        isClosed: true,
        winnerId: 'player1Id',
        currentRound: {
          id: 'round1Id',
          number: 1,
          status: 'New',
          players: [
            {
              id: 'player1Id',
              order: 1,
              thumbnail: ''
            },
            {
              id: 'player2Id',
              order: 2,
              thumbnail: ''
            },
            {
              id: 'player3Id',
              order: 3,
              thumbnail: ''
            }
          ]
        }
      }
    ]
  }
};

// GET /games/1/

const getGame1Response = { "data" : {
        "games": [
            {
                "id": 337,
                "createdAt": "2020-06-29T03:28:23.548Z",
                "updatedAt": "2020-06-29T04:44:01.882Z",
                "Name": "Game4",
                "isClosed": true,
                "currentRound": [
                    {
                        "roundNumber": 8,
                        "status": "Closed",
                        "players": [
                            {
                                "id": 1,
                                "name": "player1",
                                "email": "player1@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 6,
                                "bid": 2,
                                "score": 22,
                                "overAllScore": 208
                            },
                            {
                                "id": 3,
                                "name": "player3",
                                "email": "player3@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 2,
                                "bid": 1,
                                "score": 0,
                                "overAllScore": 0
                            },
                            {
                                "id": 7,
                                "name": "player7",
                                "email": "player7@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 1,
                                "bid": 0,
                                "score": 10,
                                "overAllScore": 101
                            },
                            {
                                "id": 10,
                                "name": "player10",
                                "email": "player10@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 3,
                                "bid": 3,
                                "score": 33,
                                "overAllScore": 307
                            },
                            {
                                "id": 2,
                                "name": "player2",
                                "email": "player2@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 4,
                                "bid": 0,
                                "score": 0,
                                "overAllScore": 0
                            },
                            {
                                "id": 5,
                                "name": "player5",
                                "email": "player5@gmail.com",
                                "thumbnail": null,
                                "createdAt": "2020-06-26T02:27:50.291Z",
                                "updatedAt": "2020-06-26T02:27:50.291Z",
                                "order": 5,
                                "bid": 2,
                                "score": 22,
                                "overAllScore": 209
                            }
                        ]
                    }
                ],
                "winnerId": 10,
                "numRounds": 8
            }
        ]
    },
    "errors": ""
}

// GET /games/1/rounds
const getGameRoundsResponse = {
    "data": {
        "rounds": [
            {
                "roundNumber": 1,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 10
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 1,
                        "score": 11,
                        "overAllScore": 11
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 10
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 1,
                        "score": 11,
                        "overAllScore": 11
                    }
                ]
            },
            {
                "roundNumber": 2,
                "status": "InProgress",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 32
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 21
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 43
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 33
                    }
                ]
            },
            {
                "roundNumber": 3,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 54
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 31
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 76
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 55
                    }
                ]
            },
            {
                "roundNumber": 4,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 76
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 41
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 109
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 77
                    }
                ]
            },
            {
                "roundNumber": 5,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 98
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 51
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 142
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 99
                    }
                ]
            },
            {
                "roundNumber": 6,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 120
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 61
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 175
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 121
                    }
                ]
            },
            {
                "roundNumber": 7,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 142
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 71
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 208
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 143
                    }
                ]
            },
            {
                "roundNumber": 8,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 208
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 101
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 307
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 209
                    }
                ]
            }
        ]
    },
    "errors": ""
}


// GET /games/1/rounds/1
const getGameRounds1Response = {
    "data": {
        "rounds": [
            {
                "roundNumber": 1,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 10
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 1,
                        "score": 11,
                        "overAllScore": 11
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 10
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 1,
                        "score": 11,
                        "overAllScore": 11
                    }
                ]
            }
        ]
    },
    "errors": ""
}


// GET /players
const getPlayersResponse = {
    "data": {
        "players": [
            {
                "id": 1,
                "name": "player1",
                "email": "player1@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 2,
                "name": "player2",
                "email": "player2@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 3,
                "name": "player3",
                "email": "player3@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 4,
                "name": "player4",
                "email": "player4@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 5,
                "name": "player5",
                "email": "player5@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 6,
                "name": "player6",
                "email": "player6@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 7,
                "name": "player7",
                "email": "player7@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 8,
                "name": "player8",
                "email": "player8@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 9,
                "name": "player9",
                "email": "player9@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            },
            {
                "id": 10,
                "name": "player10",
                "email": "player10@gmail.com",
                "thumbnail": null,
                "createdAt": "2020-06-26T02:27:50.291Z",
                "updatedAt": "2020-06-26T02:27:50.291Z"
            }
        ]
    },
    "errors": ""
}


// PATCH /games/1/rounds/1?process=start - REQUEST
const patchStartRoundRequest = {
  data: {
    rounds: [
      {
        id: 'roundId',
        players: [
          {
            id: 'player1Id',
            bid: 1
          },
          {
            id: 'player2Id',
            bid: 0
          },
          {
            id: 'player3Id',
            bid: 1
          }
        ]
      }
    ]
  }
};

// PATCH /games/1/rounds/1?process=start - RESPONSE
const patchStartRoundResponse = {
    "data": {
        "rounds": [
            {
                "roundNumber": 8,
                "status": "InProgress",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 2,
                        "score": null,
                        "overAllScore": 142
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 1,
                        "score": null,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 0,
                        "score": null,
                        "overAllScore": 71
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 3,
                        "score": null,
                        "overAllScore": 208
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 0,
                        "score": null,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 2,
                        "score": null,
                        "overAllScore": 143
                    }
                ]
            }
        ]
    },
    "errors": ""
}


// PATCH game/1/rounds/1?process=finish - REQUEST
const patchFinishRoundRequest = {
  data: {
    rounds: [
      {
        id: 'roundId',
        players: [
          {
            id: 'player1Id',
            won: true
          },
          {
            id: 'player2Id',
            won: false
          },
          {
            id: 'player3Id',
            won: false
          }
        ]
      }
    ]
  }
};


// PATCH /games/1/rounds/1?process=finish - RESPONSE
const patchFinishRoundResponse = {
    "data": {
        "rounds": [
            {
                "roundNumber": 1,
                "status": "Closed",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 22
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": 0,
                        "score": 10,
                        "overAllScore": 10
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": 1,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": 3,
                        "score": 33,
                        "overAllScore": 33
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": 0,
                        "score": 0,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": 2,
                        "score": 22,
                        "overAllScore": 22
                    }
                ]
            },
            {
                "roundNumber": 2,
                "status": "New",
                "players": [
                    {
                        "id": 1,
                        "name": "player1",
                        "email": "player1@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 6,
                        "bid": null,
                        "score": null,
                        "overAllScore": 22
                    },
                    {
                        "id": 3,
                        "name": "player3",
                        "email": "player3@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 2,
                        "bid": null,
                        "score": null,
                        "overAllScore": 0
                    },
                    {
                        "id": 7,
                        "name": "player7",
                        "email": "player7@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 1,
                        "bid": null,
                        "score": null,
                        "overAllScore": 10
                    },
                    {
                        "id": 10,
                        "name": "player10",
                        "email": "player10@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 3,
                        "bid": null,
                        "score": null,
                        "overAllScore": 33
                    },
                    {
                        "id": 2,
                        "name": "player2",
                        "email": "player2@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 4,
                        "bid": null,
                        "score": null,
                        "overAllScore": 0
                    },
                    {
                        "id": 5,
                        "name": "player5",
                        "email": "player5@gmail.com",
                        "thumbnail": null,
                        "createdAt": "2020-06-26T02:27:50.291Z",
                        "updatedAt": "2020-06-26T02:27:50.291Z",
                        "order": 5,
                        "bid": null,
                        "score": null,
                        "overAllScore": 22
                    }
                ]
            }
        ]
    },
    "errors": ""
}


export { getGames, getPlayers };
