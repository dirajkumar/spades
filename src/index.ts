import 'reflect-metadata';
import { createConnection } from 'typeorm';
import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { AppRoutes } from './routes';

require('dotenv').config()

createConnection({
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    port: process.env.PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    NODE_TLS_REJECT_UNAUTHORIZED: '0',
    ssl: true,
    ssl: {
        rejectUnauthorized: false,
    },
    logging: "all",
    entities: [
        __dirname + "/entity/*"
    ],

    synchronize: true,
}).then(async connection => {

    // create express app
    const app = express();
    app.use(express.json());

    // register all application routes
    AppRoutes.forEach(route => {
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            console.log(request.body)
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });

    // run app
    app.listen(3000);

    console.log("Express application is up and running on port 3000");

}).catch(error => console.log("TypeORM connection error: ", error));
