import { Request, Response } from 'express';
import { getManager } from 'typeorm'
import { Game } from '../entity/Game';
import { Round } from '../entity/Round';
import * as Utils from './Utils'

/**
 * Loads all players from the database.
 */

export async function getRoundById(request: Request, response: Response) {

	let roundRepository = getManager().getRepository(Round);

	let gameId = request.params.id

	let roundNumber = request.params.roundId

	let roundResponse = { "data": {}, "errors": "" }

	let validGame = Utils.validInt(gameId, roundResponse, "GameId")

	let validRoundId = Utils.validInt(roundNumber, roundResponse, "RoundNumber")

	let validInput = validGame && validRoundId

	if (!validInput) {

		response.send(roundResponse);

		return;

	}

	try {

		let roundsData = await roundRepository.find({ where: { game: gameId, roundNumber: roundNumber }, relations: ["players"] });

		if (!roundsData) {

			throw new Error("Could not get round information for game id " + gameId + " and round number " + roundNumber)

			return

		}

		roundResponse.data["rounds"] = Utils.getRoundsString(roundsData)

	} catch (err) {

		roundResponse.errors = Utils.getErrorMessage(err.code, err.message, "ERROR")

	}

	response.send(roundResponse);

}
