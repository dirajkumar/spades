import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Round } from '../entity/Round';
import { Player } from '../entity/Player';
import { Game } from '../entity/Game';
import * as Utils from '../controller/Utils';

/**
 * Loads all players from the database.
 */

function isValidRequestBody(roundsInput: String, finishRoundResponse, gameId: String, roundNumber: String) {

	let validReq = true, returnValue = true;

	let validGame = Utils.validInt(gameId, finishRoundResponse, "GameId")

	let validRoundId = Utils.validInt(roundNumber, finishRoundResponse, "RoundNumber")

	roundsInput.map(game => game.players).flat(2).map(player => {

		if (!Utils.validBoolean(player.won, finishRoundResponse, "boolean")) {

			validReq = false

		}

		if (!Utils.validInt(player.id, finishRoundResponse, "playerId")) {

			validReq = false

		}
	});

	let validInput = validGame && validRoundId && validReq

	if (!validInput) {

		returnValue = false

	}

	return returnValue

}

function checkRoundStatus(roundsData) {

	let roundStatus = Array.from(new Set(roundsData.map(item => item.status))).toString()

	if (roundStatus != "InProgress") {

		throw new Error("This round cannot be closed, as it is in " + roundStatus + " state")

	}

}

function getNewOrderMap(roundsData) {

	var orderMap = new Map(roundsData.map(item => {

		let order = (item.order === 1) ? roundsData.length : item.order - 1

		let id = parseInt(item.players.flat(2).map(player => player.id).flat().toString())

		return [id, order]

	}));

	return orderMap


}

export async function finishRound(request: Request, response: Response) {

	let roundRepository = getManager().getRepository(Round);

	let gameRepository = getManager().getRepository(Game);

	let finishRoundResponse = { "data": {}, "errors": "" }

	let gameId = request.params.id;

	let roundNumber = request.params.roundId;

	let roundsInput = request.body.data.rounds;

	let validInput = isValidRequestBody(roundsInput, finishRoundResponse, gameId, roundNumber)

	if (!validInput) {

		response.send(finishRoundResponse);

		return;

	}

	let roundNo = parseInt(request.params.roundId);

	try {

		var players = new Map(roundsInput.map(player => player.players).flat(2).map(i => [parseInt(i.id), i.won]))

		var playerIds = Array.from(players.keys());

		let rounds = await getManager().getRepository("Round").createQueryBuilder("Round")

			.leftJoinAndSelect("Round.players", "players")

			.leftJoinAndSelect("Round.game", "game")

			.where(" Round.game = :gameId", { gameId: gameId })

			.andWhere(" Round.roundNumber = :roundNumber", { roundNumber: roundNo })

			.andWhere("players.id IN (:...playerIds)", { playerIds: playerIds })

			.getMany();


		let numRounds = Math.floor(52 / rounds.length)

		let games = await gameRepository.findByIds(gameId)

		let roundsData = rounds.flat(2);

		checkRoundStatus(roundsData)

		let orderMap = getNewOrderMap(roundsData)

		let roundUpdatedScores = Utils.updateScores(roundsData, players)

		await getManager().transaction(async transactionalEntityManager => {

			await transactionalEntityManager.save(roundUpdatedScores)

			var overAllScoresMap = new Map(roundUpdatedScores.map(item => [parseInt(item.players.flat(2).map(player => player.id).flat().toString()), item.overAllScore]))

			console.log(overAllScoresMap)

			if (numRounds != roundNo) {

				let newRoundData = Utils.createNewRound(playerIds, gameId, roundNo + 1, orderMap, overAllScoresMap)

				await transactionalEntityManager.save(newRoundData)

				let updatedGame = games.map(game => { game.currentRound = roundNo + 1; return game });

			}

			else {

				var winnerId = playerIds.reduce((a, b) => overAllScoresMap.get(a) > overAllScoresMap.get(b) ? a : b, {})

				let updatedGame = games.map(game => { game.winnerId = winnerId; game.isClosed = true; return game });

			}


			await transactionalEntityManager.save(updatedGame)

		});

		let newRounds = await roundRepository.find({ where: { game: gameId, roundNumber: roundNo + 1 }, relations: ["players"] });

		finishRoundResponse.data["rounds"] = [Utils.getRoundsString(roundUpdatedScores)[0], Utils.getRoundsString(newRounds)[0]]

	} catch (error) {

		finishRoundResponse.errors = Utils.getErrorMessage(error.code, error.message, "ERROR")

	}

	response.send(finishRoundResponse)

}
