import { Request, Response } from 'express';
import { startRound } from './StartRound';
import { finishRound } from './FinishRound';

/**
 * Loads all players from the database.
 */


export async function processRound(request: Request, response: Response) {

    let roundAction = request.query.process

    console.log(roundAction)

    roundAction == "start" ? startRound(request, response) : finishRound(request, response)

}
