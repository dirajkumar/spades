import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Player } from '../entity/Player';
import * as Utils from './Utils';

/**
 * Loads all players from the database.
 */

export async function getPlayers(request: Request, response: Response) {

	let playersResponse = { "data": { "players": "" }, "errors": "" }

	let playerRepository = getManager().getRepository(Player);

	try {

		let players = await playerRepository.find();

		playersResponse.data.players = players


	} catch (err) {

		playersResponse.errors = Utils.getErrorMessage(err.name, err.message, "ERROR")

	}

	response.send(playersResponse);

}
