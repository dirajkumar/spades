import { Request, Response } from 'express';
import { getManager } from 'typeorm'
import { Round } from '../entity/Round';
import * as Utils from './Utils';

/**
 * Loads all players from the database.
 */

export async function getGameRounds(request: Request, response: Response) {

	let roundRepository = getManager().getRepository(Round);

	let roundsResponse = { "data": {}, "errors": "" }

	let gameId = request.params.id

	if (!Utils.validInt(gameId, roundsResponse, "GameId")) {

		response.send(roundsResponse);

		return;

	}

	try {

		let roundsData = await roundRepository.find({ where: { game: gameId }, relations: ["players"] });

		if (!roundsData) {

			throw new Error("Game " + gameId + " not found in db")

			return

		}

		roundsResponse.data["rounds"] = Utils.getRoundsString(roundsData)


	} catch (err) {

		roundsResponse.errors = Utils.getErrorMessage(err.name, err.message, "ERROR")

	}

	response.send(roundsResponse)

}
