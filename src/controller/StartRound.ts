import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Round } from '../entity/Round';
import { updateBidValue } from '../controller/Utils'
import * as Utils from '../controller/Utils'

/**
 * Loads all players from the database.
 */


export function isValidRequestBody(roundsInput, startRoundResponse, gameId, roundNumber) {

	let validReq = true, returnValue = true;

	let validGame = Utils.validInt(gameId, startRoundResponse, "GameId")

	let validRoundId = Utils.validInt(roundNumber, startRoundResponse, "RoundNumber")

	roundsInput.map(game => game.players).flat(2).map(player => {

		if (!Utils.validInt(player.bid, startRoundResponse, "Bid")) {

			validReq = false

		}

		if (!Utils.validInt(player.id, startRoundResponse, "playerId")) {

			validReq = false

		}
	});

	let validInput = validGame && validRoundId && validReq

	if (!validInput) {

		returnValue = false

	}

	return returnValue

}



export async function startRound(request: Request, response: Response) {

	// get a player repository to perform operations with player

	let roundRepository = getManager().getRepository(Round);

	let gameId = request.params.id;

	let roundNumber = request.params.roundId;

	let rounds = request.body.data.rounds;

	console.log(rounds)

	// Validate inputs

	let startRoundResponse = { "data": {}, "errors": "" }

	let validInput = isValidRequestBody(rounds, startRoundResponse, gameId, roundNumber)

	if (!validInput) {

		response.send(startRoundResponse);

		return;

	}

	try {

		var playerBidMap = new Map(rounds.map(player => player.players).flat(2).map(i => [i.id, i.bid]))

		var playerIds = Array.from(playerBidMap.keys());

		let roundsData = await getManager().getRepository("Round").createQueryBuilder("Round")

			.leftJoinAndSelect("Round.players", "players")

			.leftJoinAndSelect("Round.game", "game")

			.where(" Round.game = :gameId", { gameId: gameId })

			.andWhere(" Round.roundNumber = :roundNumber", { roundNumber: roundNumber })

			.andWhere("players.id IN (:...playerIds)", { playerIds: playerIds })

			.getMany();


		let roundBidVals = roundsData.flat(2).map(item => updateBidValue(item, playerBidMap))

		await roundRepository.save(roundBidVals)

		let roundsArray = await roundRepository.find({ where: { game: gameId, roundNumber: roundNumber }, relations: ["players"] });

		startRoundResponse.data["rounds"] = Utils.getRoundsString(roundsArray)

	} catch (err) {

		startRoundResponse.errors = Utils.getErrorMessage(err.name, err.message, "ERROR")

	}

	response.send(startRoundResponse)

}
