import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Game } from '../entity/Game';
import { Round } from '../entity/Round';
import { Player } from '../entity/Player';
import bodyParser from 'body-parser';
import * as Utils from '../controller/Utils';


/**
 * Loads all players from the database.
 */

export function isValidRequestBody(gamesArray, gameResponse) {

	let isValid = true

	gamesArray.map(game => game.players).flat(2).map(player => {

		if (!Utils.validInt(player.order, gameResponse, "playerOrder")) {

			isValid = false

		}

		if (!Utils.validInt(player.id, gameResponse, "playerId")) {

			isValid = false

		}
	});

	return isValid

}

export async function postGame(request: Request, response: Response) {

	// get all repositories to perform operations with player

	let gameRepository = getManager().getRepository(Game);

	let roundRepository = getManager().getRepository(Round);

	let playerRepository = getManager().getRepository(Player);

	let gameResponse = { "data": {}, "errors": "" }

	let gamesArray = request.body.data.games;

	if (!isValidRequestBody(gamesArray, gameResponse)) {

		response.send(gameResponse);

		return

	}

	try {

		let recentDate = await gameRepository.createQueryBuilder("games").select("MAX(games.createdAt)", "max").where("DATE(games.createdAt) = CURRENT_DATE ");

		let recentGameIndex = await gameRepository.createQueryBuilder("games").where("games.createdAt IN(" + recentDate.getQuery() + ")").select("MAX(SUBSTRING(games.Name,5))", "value").getRawOne();

		let playersOrderMap = new Map(gamesArray.map(player => player.players).flat(2).map(player => [player.id, player.order]))

		let playerIds = Array.from(playersOrderMap.keys())

		let overAllScoresMap = new Map(playerIds.map(player => [player, 0]))

		let roundNumber = 1;

		let newGame = Utils.createNewGame(recentGameIndex);

		await getManager().transaction(async transactionalEntityManager => {

			await transactionalEntityManager.save(newGame);

			let roundsInfo = Utils.createNewRound(playerIds, newGame.id, roundNumber, playersOrderMap, overAllScoresMap)

			await transactionalEntityManager.save(roundsInfo);

		});

		let roundsData = await roundRepository.find({ where: { game: newGame.id }, relations: ["players"] });

		let gamesData = await gameRepository.find({ where: { "id": newGame.id } });

		gameResponse.data["games"] = Utils.getGamesString(gamesData, roundsData, playerIds.length)


	} catch (err) {

		gameResponse.errors = Utils.getErrorMessage(err.code, err.message, "ERROR")

	}

	response.send(gameResponse)

}
