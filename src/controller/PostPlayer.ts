import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Player } from '../entity/Player';

/**
 * Loads all players from the database.
 */

export async function postPlayer(request: Request, response: Response) {

  // get a player repository to perform operations with player
  let playerRepository = getManager().getRepository(Player);

  let player1 = new Player();

  // Set Player name and email 

  player1.name = request.query.name;

  player1.email = request.query.email;

  player1.thumbnail = request.query.thumbnail;

  await playerRepository.save(player1);

  console.log("Player1 information saved to database");

}
