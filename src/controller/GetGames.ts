import { Request, Response } from 'express';
import { getManager } from 'typeorm'
import { Game } from '../entity/Game';
import { Round } from '../entity/Round';
import * as Utils from './Utils';

/**
 * Loads all players from the database.
 */

export async function getGames(request: Request, response: Response) {

	let gameRepository = getManager().getRepository(Game);

	let roundRepository = getManager().getRepository(Round);

	let gamesResponse = { "data": {}, "errors": "" }

	let limitString = ""

	if(request.query.limit){

		limitString="{take : request.query.limit}"

	}

	try {


	let gamesData = await getManager().query(`select * from (round join round_players_player on round.id = "roundId" ) join player on "playerId" = player.id join game on "gameId" = game.id where "currentRound" = "roundNumber"`);


		console.log(gamesData)



	} catch (err) {

		gamesResponse.errors = Utils.getErrorMessage(err.name, err.message, "ERROR")

	}

	response.send(gamesData)

}
