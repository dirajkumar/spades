import { Request, Response } from 'express';
import { getManager } from 'typeorm'
import { Game } from '../entity/Game';
import { Round } from '../entity/Round';
import { Player } from '../entity/Player';


export function updateScores(roundsData, players) {

  let roundsWithScores = roundsData.map(round => {

    let playerId = parseInt(round.players.flat(2).map(player => player.id).toString())

    let playerWon = players.get(playerId)

    round.score = playerWon ?

      (round.bid === 0) ? 10 : round.bid * 11

      : 0

    round.overAllScore = round.score + round.overAllScore

    round.status = "Closed"

    return round

  });

  return roundsWithScores;


}

export function updateBidValue(item, playerBidMap) {

  let playerId = parseInt(item.players.flat().map(player => player.id).toString())

  item.bid = playerBidMap.get(playerId)

  item.status = "InProgress"

  delete item['players']

  delete item['game']

  console.log(item)

  return item;

}


export function createNewRound(playerIds, gameId, roundNumber, orderMap, scoresMap) {

  let newRoundRows = playerIds.map(playerId => {

    var newRound = new Round();

    newRound.roundNumber = roundNumber;

    newRound.status = "New";

    newRound.game = gameId;

    let order = orderMap.get(playerId);

    newRound.order = order

    newRound.overAllScore = scoresMap.get(playerId);

    let player = new Player();

    player.id = playerId;

    newRound.players = [player];

    return newRound;

  });

  return newRoundRows
}

export function getGamesString(gamesData, roundsData, numPlayers) {

  let roundsString = getRoundsString(roundsData)

  let numRounds = Math.floor(52 / numPlayers)

  let gamesString = gamesData.map(game => { game["numRounds"] = numRounds; game["currentRound"] = roundsString; return game })

  return gamesString;

}

export function createNewGame(gameIndex) {

  let gameIndex = parseInt(gameIndex.value) + 1;

  let newGame = new Game();

  newGame.Name = (!gameIndex) ? "Game1" : "Game" + gameIndex;

  console.log(newGame.Name)

  newGame.isClosed = false;

  newGame.currentRound = 1;

  return newGame;

}

export function getRoundsString(roundsData) {

  var roundsResponse = {};

  roundsData.flat(2).forEach(round => {

    var roundNo = round.roundNumber.toString()

    var playerInfo = round.players.flat(2).map(player => {

      let response = {};

      response["id"] = player.id;

      response["name"] = player.name;

      response["email"] = player.email;

      response["thumbnail"] = player.thumbnail;

      response["createdAt"] = player.createdAt;

      response["updatedAt"] = player.updatedAt;

      response["order"] = round.order;

      response["bid"] = round.bid;

      response["score"] = round.score;

      response["overAllScore"] = round.overAllScore;

      return response


    });

    if (!roundsResponse[roundNo]) {

      roundsResponse[roundNo] = { "roundNumber": round.roundNumber, "status": round.status, players: [] }

    }

    roundsResponse[roundNo].players.push(playerInfo)

  });

  var responseString = Object.values(roundsResponse).flat(2).map(item => {

    item.players = item.players.flat(2)

    return item

  });

  return responseString;

}


export function getErrorMessage(errName, errorMessage, errorLevel) {

  let errResponse = { "code": "", "message": "", "type": "" }

  errResponse.code = errName

  errResponse.message = errorMessage

  errResponse.type = errorLevel

  return [errResponse];

}


export function validInt(value, getGame1Response, colName) {

  var returnValue = true;

  try {

    if (value === "" || value.length === 0 || value === null || isNaN(value)) {

      returnValue = false

      throw new Error(value + " is not a valid " + colName)


    }

  } catch (err) {

    if (getGame1Response.errors.length !== 0) {

      getGame1Response.errors.push(getErrorMessage(err.code, err.message, "ERROR")[0])

    }

    else {

      getGame1Response.errors = getErrorMessage(err.code, err.message, "ERROR")

    }

  }

  return returnValue;

}


export function validBoolean(value, getGame1Response, colName) {

  var returnValue = true;

  try {

    if (!["true", "false", true, false].includes(value)) {

      returnValue = false

      throw new Error(value + " is not a valid " + colName)

    }

  } catch (err) {

    if (getGame1Response.errors.length !== 0) {

      getGame1Response.errors.push(getErrorMessage(err.code, err.message, "ERROR")[0])

    }

    else {

      getGame1Response.errors = getErrorMessage(err.code, err.message, "ERROR")

    }

  }

  console.log("end")

  return returnValue;

}
