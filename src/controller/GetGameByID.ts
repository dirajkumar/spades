import { Request, Response } from 'express';
import { getManager } from 'typeorm'
import { Game } from '../entity/Game';
import { Round } from '../entity/Round';
import * as Utils from '../controller/Utils';

/**
 * Loads all players from the database.
 */

export async function getGameByID(request: Request, response: Response) {

	let gameRepository = getManager().getRepository(Game);

	let roundRepository = getManager().getRepository(Round);

	let gameResponse = { "data": {}, "errors": "" }

	let gameId = request.params.id

	if (!Utils.validInt(gameId, gameResponse, "GameId")) {

		response.send(gameResponse);

		return;

	}

	try {

		let gamesArray = await gameRepository.find({ where: { id: gameId } });

		if (!gamesArray) {

			throw new Error("Game " + gameId + " not found in db")

			return

		}

		let currentRound = parseInt(gamesArray.map(item => item.currentRound).toString());

		let roundsData = await roundRepository.find({ where: { game: gameId, roundNumber: currentRound }, relations: ["players"] });

		gameResponse.data["games"] = Utils.getGamesString(gamesArray, roundsData, roundsData.length)


	} catch (err) {

		gameResponse.errors = Utils.getErrorMessage(err.name, err.message, "ERROR")

	}

	response.send(gameResponse);


}
