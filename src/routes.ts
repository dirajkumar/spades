import { getPlayers } from './controller/GetPlayers';
import { postPlayer } from './controller/PostPlayer';
import { getGameByID } from './controller/GetGameByID';
import { postGame } from './controller/PostGame';
import { startRound } from './controller/StartRound';
import { finishRound } from './controller/FinishRound';
import { processRound } from './controller/ProcessRound';
import { getGameRounds } from './controller/GetGameRounds';
import { getRoundById } from './controller/GetRoundById';
import { getGames } from './controller/GetGames';

/**
 * All application routes.
 */

export const AppRoutes = [
  {
    path: '/players',
    method: 'get',
    action: getPlayers
  },
  {
    path: '/players',
    method: 'post',
    action: postPlayer
  },
  {
    path: '/games',
    method: 'post',
    action: postGame
  },
  {
    path: '/games/:id',
    method: 'get',
    action: getGameByID
  },
  {
    path: '/games/:id/rounds',
    method: 'get',
    action: getGameRounds
  },
  {
    path: '/games/:id/rounds/:roundId',
    method: 'get',
    action: getRoundById
  },
  {
    path: '/games',
    method: 'get',
    action: getGames
  },
  {
    path: '/games/:id/rounds/:roundId',
    method: 'patch',
    action: processRound
  }
];
