import { ManyToMany, Entity, CreateDateColumn, UpdateDateColumn, OneToMany, JoinColumn, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Round } from "./Round";
@Entity()
export class Player {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: "text" })
  name!: string;

  @Column({ type: "text" , unique: true})
  email!: string;

  @Column({ type: "text" , nullable: true })
  thumbnail!: string;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt!: string;

  @UpdateDateColumn({ type: "timestamptz" })
  updatedAt!: string;
 
}
