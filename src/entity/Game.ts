import { Entity, OneToMany, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Round } from "./Round";

@Entity()
export class Game {

  @PrimaryGeneratedColumn()
  id!: number;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamptz" })
  updatedAt!: Date;

  @Column({ type: "text" })
  Name!: string;

  @Column()
  isClosed!: boolean;

  @Column({ type: "smallint" })
  currentRound!: number; 

  @Column({ type: "smallint", nullable: true})
  winnerId!: number[];

  @OneToMany(Type => Round, rounds => rounds.game)
  rounds: Round[];

}
