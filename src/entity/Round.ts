import { PrimaryColumn, Entity, ManyToMany, JoinColumn, ManyToOne, JoinTable, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { Player } from "./Player";
import { Game } from "./Game";

@Entity()
export class Round {

  @PrimaryGeneratedColumn()
  id!: number;

  @Column({type: "smallint"})
  roundNumber!: number;

  @CreateDateColumn({ type: "timestamptz" })
  createdAt!: string;

  @UpdateDateColumn({ type: "timestamptz" })
  updatedAt!: string;

  @Column({type: "smallint" ,nullable: true})
  bid!: number;

  @Column({type: "smallint",nullable: true})
  score!: number; 

  @Column({type: "smallint",nullable: true})
  overAllScore!: number;

  @Column({ type: "text" ,nullable: true})
  status!: string;

  @Column({ type: "smallint"})
  order!: number;

  @ManyToOne(type => Game, game => game.rounds)
  @JoinColumn({name : "gameId"})
  game: Game;

  @ManyToMany(type => Player)
  @JoinTable()
  players : Player[];

}
